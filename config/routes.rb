Rails.application.routes.draw do
  devise_for :users, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout' }
  resources :proponents, path: 'proponentes', path_names: { new: "novo", edit: "editar" }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post '/proponents/calculate_discount', to: 'proponents#calculate_discount'
  post '/proponents/cities_by_state', to: 'proponents#cities_by_state'

  root to: 'proponents#index'
end
