class ProponentsController < ApplicationController
  before_action :set_proponent, only: [:show, :edit, :update, :destroy]
  
  def calculate_discount
    salary = params[:proponent][:salary].gsub(/[.,]/, '.' => '', ',' => '.').to_d

    if ( salary <= Proponent::FIRST_RANGE[:max] )
      discount = apply_aliquot(1, salary)
    elsif ( salary > Proponent::SECOND_RANGE[:min] && salary <= Proponent::SECOND_RANGE[:max] )
      discount = apply_aliquot(2, salary)
    elsif ( salary > Proponent::THIRD_RANGE[:min] && salary <= Proponent::THIRD_RANGE[:max] )
      discount = apply_aliquot(3, salary)
    elsif ( salary > Proponent::FOURTH_RANGE[:min] && salary <= Proponent::FOURTH_RANGE[:max] )
      discount = apply_aliquot(4, salary)
    end

    render json: { discount: discount.truncate(2) }
  end

  def cities_by_state
    render json: { cities: City.select(:name, :id).where(state_id: params[:proponent][:state]).order(:name) }
  end

  def index
    @proponents = Proponent.all
  end

  def show
  end

  def new
    @proponent = Proponent.new
    @proponent.build_address
  end

  def edit
    @state = @proponent.address.city.state.id
  end
  
  def create
    @proponent = Proponent.new(proponent_params)
    
    if @proponent.save
      redirect_to @proponent, notice: t('controllers.proponents.created')
    else
      @state = params[:state]
      render :new
    end
  end
  
  def update
    if @proponent.update(proponent_params)
      redirect_to @proponent, notice: t('controllers.proponents.updated')
    else
      @state = params[:state]
      render :edit
    end
  end

  def destroy
    @proponent.destroy
      redirect_to proponents_url, notice: t('controllers.proponents.destroyed')
  end

  private
    def set_proponent
      @proponent = Proponent.find(params[:id])
    end

    def proponent_params
      params.require(:proponent).permit(:address_id, :name, :cpf, :date_birth, :salary, :discount,
        address_attributes: [:id, :public_place, :city_id, :number, :neighborhood, :cep],
        telephones_attributes: [:id, :value, :other, :kind, :_destroy])
    end

    def apply_aliquot(range, salary)
      case range
        when 1
          discount = Proponent::RANGES.map{|r| r[:aliquot] if r[:range] == 1}.compact.sum
        when 2
          discount = ((salary - Proponent::SECOND_RANGE[:min]) * Proponent::SECOND_RANGE[:aliquot]).truncate(2)
          discount += Proponent::RANGES.map{|r| r[:aliquot] if r[:range] < 2}.compact.sum
        when 3
          discount = ((salary - Proponent::THIRD_RANGE[:min]) * Proponent::THIRD_RANGE[:aliquot]).truncate(2)
          discount += Proponent::RANGES.map{|r| r[:aliquot] if r[:range] < 3}.compact.sum
        when 4
          discount = ((salary - Proponent::FOURTH_RANGE[:min]) * Proponent::FOURTH_RANGE[:aliquot]).truncate(2)
          discount += Proponent::RANGES.map{|r| r[:aliquot] if r[:range] < 4}.compact.sum
      end
  
      return discount
    end
end
