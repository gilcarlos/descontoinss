function maskCEP(cep) {
  if (maskInt(cep) == false) {
    event.returnValue = false;
  }
  return formataCampo(cep, '00.000-000', event);
}

function maskTelephone(key) {
  if (maskInt(key) == false) {
    event.returnValue = false;
  }
  key.value = formatTelephone(key.value);
}

function maskCPF(cpf) {
  if (maskInt(cpf) == false) {
    event.returnValue = false;
  }
  return formataCampo(cpf, '000.000.000-00', event);
}

function maskDate(e) {
  let value = e.target.value;
  value = value.replace( /\D/g , "");
  value = value.replace( /(\d{2})(\d)/, "$1/$2");
  value = value.replace( /(\d{2})(\d)/ , "$1/$2");
  e.target.value = value;
}

function formatTelephone(value) {
  value = value.replace(/\D/g,"");
  value = value.replace(/^(\d{2})(\d)/g,"($1) $2");
  value = value.replace(/(\d)(\d{4})$/,"$1-$2");
  return value;
}

function maskInt() {
  if (event.keyCode < 48 || event.keyCode > 57) {
    event.returnValue = false;
    return false;
  }
  return true;
}

//formata de forma generica os campos
function formataCampo(campo, mascara, evento) {
  let boleanoMascara;

  let digitato = evento.keyCode;
  let exp = /\-|\.|\/|\(|\)| /g
  let campoSoNumeros = campo.value.toString().replace(exp, "");

  let posicaoCampo = 0;
  let NovoValorCampo = "";
  let TamanhoMascara = campoSoNumeros.length;

  if (digitato != 8) { // backspace 
    for (let i = 0; i <= TamanhoMascara; i++) {
      boleanoMascara = ((mascara.charAt(i) == "-") || (mascara.charAt(i) == ".") || (mascara.charAt(i) == "/"))
      boleanoMascara = boleanoMascara || ((mascara.charAt(i) == "(") || (mascara.charAt(i) == ")") || (mascara.charAt(i) == " "))
      if (boleanoMascara) {
        NovoValorCampo += mascara.charAt(i);
        TamanhoMascara++;
      } else {
        NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
        posicaoCampo++;
      }
    }
    campo.value = NovoValorCampo;
    return true;
  } else {
    return true;
  }
}