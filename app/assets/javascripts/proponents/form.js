const token = document.querySelector("meta[name='csrf-token']").content;
const headers = { "Content-Type": "application/json", "X-CSRF-Token": token };

document.addEventListener('DOMContentLoaded', () => {
  const button = document.querySelector('.js-calculate');
  button.addEventListener("click", calculate);

  const selectState = document.querySelector('.js-selectState');
  selectState.addEventListener("change", getCities);

  $(".js-salary").maskMoney({ allowNegative: true, thousands:'.', decimal:',', affixesStay: false });

  const dateElement = document.querySelector('.js-dateformat');
  dateElement.addEventListener("keyup", maskDate);

  document.querySelectorAll('input[data-mask]').forEach( field => {
    field.addEventListener('keyup', formatFields);
  });

  $('#telephones').on('cocoon:after-insert', function(e, added_el) {
    const element = added_el.find('input');
    element.first().focus();
    const htmlElement = element.first().get(0);
    htmlElement.addEventListener('keyup', function() {
      maskTelephone(this)
    });
  });
});

async function calculate() {
  const salaryElement = document.querySelector('.js-salary')
  const value = await fetch("/proponents/calculate_discount", {
    method: "POST",
    headers,
    body: JSON.stringify({ proponent: { salary: salaryElement.value } } )
  });

  const { discount } = await value.json();

  const element = document.querySelector('.js-discount');
  element.value = Number(discount).toLocaleString('pt-BR');
}

async function getCities() {
  const selectState = document.querySelector('.js-selectState');
  if (selectState.value == '') {
    clearSelectOptions( document.querySelector('.js-selectCity') );
    return false;
  }

  const value = await fetch("/proponents/cities_by_state", {
    method: "POST",
    headers,
    body: JSON.stringify({ proponent: { state: selectState.value } } )
  });

  const { cities } = await value.json();
  handleSelectCity(cities);
}

function handleSelectCity( cities ) {
  const selectCity = document.querySelector('.js-selectCity');

  clearSelectOptions(selectCity);
  cities.forEach(({name ,id}) => {
    let option = new Option(name, id);
    selectCity.add(option, undefined)
  });
}

function clearSelectOptions(element) {
  while (element.options.length) {
    element.remove(0);
  }
  element.add(new Option('Selecione', ''));
}

function formatFields() {
  switch(this.dataset.mask) {
    case 'cpf':
      maskCPF(this);
    break;
    case 'cnpj':
      maskCNPJ(this);
    break;
    case 'cep':
      maskCEP(this);
    break;
    case 'telephone':
      maskTelephone(this);
    break;
  }
}
