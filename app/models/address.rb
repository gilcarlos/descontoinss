class Address < ApplicationRecord
  belongs_to :city

  validates :public_place, :number, :neighborhood, :cep, presence: true
end
