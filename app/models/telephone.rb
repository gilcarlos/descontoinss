class Telephone < Contact
  ##### VALIDATIONS
  validates :value, :kind, presence: true
end