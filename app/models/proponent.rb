class Proponent < ApplicationRecord
  belongs_to :address
  has_many :telephones, class_name: 'Telephone', dependent: :destroy
  
  accepts_nested_attributes_for :telephones, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :address, allow_destroy: true

  ##### VALIDATIONS
  validates :name, :cpf, :date_birth, :salary, :discount, presence: true

  ##### CONSTANTS
  FIRST_RANGE = { min: nil, max: 1045, aliquot: 0.075 }
  SECOND_RANGE = { min: 1045.00, max: 2089.60, aliquot: 0.09 }
  THIRD_RANGE = { min: 2089.60, max: 3134.40, aliquot: 0.12 }
  FOURTH_RANGE = { min: 3134.40, max: 6101.06, aliquot: 0.14 }
  RANGES = [
    { range: 1, aliquot: (FIRST_RANGE[:max] * FIRST_RANGE[:aliquot]).truncate(2) },
    { range: 2, aliquot: ((SECOND_RANGE[:max] - SECOND_RANGE[:min]) * SECOND_RANGE[:aliquot]).truncate(2) },
    { range: 3, aliquot: ((THIRD_RANGE[:max] - THIRD_RANGE[:min]) * THIRD_RANGE[:aliquot]).truncate(2) },
    { range: 4, aliquot: ((FOURTH_RANGE[:max] - FOURTH_RANGE[:min]) * FOURTH_RANGE[:aliquot]).truncate(2) }
  ]

  def salary=(value)
    write_attribute :salary, value.gsub(/\./, '').gsub(/\,/, '.').to_d
  end

  def discount=(value)
    write_attribute :discount, value.gsub(/\./, '').gsub(/\,/, '.').to_d
  end

end
