class Contact < ApplicationRecord
  belongs_to :proponent

  ##### CONSTANTS
  OWN = { id: 1, description: "Pessoal" }
  OTHER = { id: 2, description: "Referência" }
  KINDS = [ OWN, OTHER ]

  def self.get_human_kinds(kind)
    KINDS.each do |k|
      if kind == k[:id]
        return k[:description]
      end
    end
  end

end
