# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "##### STATES"
BAHIA = State.create(name: "Bahia")
CEARA = State.create(name: "Ceará")
MARANHAO = State.create(name: "Maranhão")
PERNAMBUCO = State.create(name: "Pernambuco")
PIAUI = State.create(name: "Piauí")

puts "##### CITIES"
TERESINA = City.create(state: PIAUI, name: "Teresina")
PARNAIBA = City.create(state: PIAUI, name: "Parnaíba")

FORTALEZA = City.create(state: CEARA, name: "Fortaleza")
CAUCAIA = City.create(state: CEARA, name: "Caucaia")

SAO_LUIS = City.create(state: MARANHAO, name: "São Luís")
TIMON = City.create(state: MARANHAO, name: "Timon")

SALVADOR = City.create(state: BAHIA, name: "Salvador")
FEIRA_DE_SANTANA = City.create(state: BAHIA, name: "Feira de Santana")

RECIFE = City.create(state: PERNAMBUCO, name: "Recife")
OLINDA = City.create(state: PERNAMBUCO, name: "Olinda")

puts "##### ADDRESSES"
ADDRESS_01 = Address.create(city: TERESINA, public_place: "Rua Passos", number: 1234, neighborhood: "Santo Antonio", cep: "64.947-648")
ADDRESS_02 = Address.create(city: PARNAIBA, public_place: "Rua São José", number: 1928, neighborhood: "São José", cep: "64.837-381")
ADDRESS_03 = Address.create(city: FORTALEZA, public_place: "Rua Beatriz", number: 1283, neighborhood: "Alagadiço", cep: "60.862-700")
ADDRESS_04 = Address.create(city: CAUCAIA, public_place: "Rua Q23-A", number: nil, neighborhood: "Açude", cep: "61.601-110")
ADDRESS_05 = Address.create(city: SAO_LUIS, public_place: "Avenida Vaticano", number: 3821, neighborhood: "Alemanha", cep: " 65.085-255")
ADDRESS_06 = Address.create(city: TIMON, public_place: "Quadra 01", number: nil, neighborhood: "Jóia", cep: "65.632-376")
ADDRESS_07 = Address.create(city: SALVADOR, public_place: "Acesso 01", number: 1384, neighborhood: "Castelo Branco", cep: "41.321-205")
ADDRESS_08 = Address.create(city: FEIRA_DE_SANTANA, public_place: "Via Pedestre 10", number: 9483, neighborhood: "Calumbi", cep: "44.009-320")
ADDRESS_09 = Address.create(city: RECIFE, public_place: "3ª Travessa A", number: 9302, neighborhood: "Campo Grande", cep: "52.031-216")
ADDRESS_10 = Address.create(city: OLINDA, public_place: "Rua A", number: 9281, neighborhood: "Jardim Atlântico", cep: "53.050-450")

def gCPF
  cpf = ""
  1..11.times do |n|
    cpf += "." if n == 3 || n == 6
    cpf += "-" if n == 9
    cpf += rand(1..9).to_s
  end
  return cpf
end

def gDate
  Time.at(rand * (Time.current.to_f)).strftime('%d/%m/%Y')
end

# def gSalary
#   salary = 0
#   salary = rand(1000.00..6000.99).round(2)
# end

def set_discount(salary)
  if ( salary <= Proponent::FIRST_RANGE[:max] )
    discount = apply_aliquot(1, salary)
  elsif ( salary > Proponent::SECOND_RANGE[:min] && salary <= Proponent::SECOND_RANGE[:max] )
    discount = apply_aliquot(2, salary)
  elsif ( salary > Proponent::THIRD_RANGE[:min] && salary <= Proponent::THIRD_RANGE[:max] )
    discount = apply_aliquot(3, salary)
  elsif ( salary > Proponent::FOURTH_RANGE[:min] && salary <= Proponent::FOURTH_RANGE[:max] )
    discount = apply_aliquot(4, salary)
  end
  return discount
end

def apply_aliquot(range, salary)
  case range
    when 1
      discount = Proponent::RANGES.map{|r| r[:aliquot] if r[:range] == 1}.compact.sum
    when 2
      discount = ((salary - Proponent::SECOND_RANGE[:min]) * Proponent::SECOND_RANGE[:aliquot]).truncate(2)
      discount += Proponent::RANGES.map{|r| r[:aliquot] if r[:range] < 2}.compact.sum
    when 3
      discount = ((salary - Proponent::THIRD_RANGE[:min]) * Proponent::THIRD_RANGE[:aliquot]).truncate(2)
      discount += Proponent::RANGES.map{|r| r[:aliquot] if r[:range] < 3}.compact.sum
    when 4
      discount = ((salary - Proponent::FOURTH_RANGE[:min]) * Proponent::FOURTH_RANGE[:aliquot]).truncate(2)
      discount += Proponent::RANGES.map{|r| r[:aliquot] if r[:range] < 4}.compact.sum
  end

  return discount
end

puts  "##### PROPONENTS"
proponents = Proponent.create([
  { address: ADDRESS_01, name: "Alice Santiago", cpf: gCPF, date_birth: gDate, salary: 800.00, discount: set_discount(800.00) },
  { address: ADDRESS_02, name: "Helena Bernard", cpf: gCPF, date_birth: gDate, salary: 1046.50, discount: set_discount(1046.50) },
  { address: ADDRESS_03, name: "Laura Valentina", cpf: gCPF, date_birth: gDate, salary: 2500.00, discount: set_discount(2500.00) },
  { address: ADDRESS_04, name: "Manuel Gonçalves", cpf: gCPF, date_birth: gDate, salary: 3000.00, discount: set_discount(3000.00) },
  { address: ADDRESS_05, name: "Heloísa Júlia", cpf: gCPF, date_birth: gDate, salary: 4550.00, discount: set_discount(4550.00) },
  { address: ADDRESS_06, name: "Maria Luiza", cpf: gCPF, date_birth: gDate, salary: 2100.00, discount: set_discount(2100.00) },
  { address: ADDRESS_07, name: "Lívia Benjamin", cpf: gCPF, date_birth: gDate, salary: 2850.00, discount: set_discount(2850.00) },
  { address: ADDRESS_08, name: "Maria Eduarda", cpf: gCPF, date_birth: gDate, salary: 5000.00, discount: set_discount(5000.00) },
  { address: ADDRESS_09, name: "Maria Joaquina", cpf: gCPF, date_birth: gDate, salary: 6000.00, discount: set_discount(6000.00) },
  { address: ADDRESS_10, name: "Elano Fernandes", cpf: gCPF, date_birth: gDate, salary: 5500.00, discount: set_discount(5500.00) }
])

def gContact(type)
  numero = "(86)"
  case type
    when :tel
      numero += "32"
      1..6.times do |n|
        numero += "-" if n == 2
        numero += rand(1..9).to_s
      end
    when :cel
      numero += "9"
      1..4.times do |n|
        numero += "-" if n == 2
        numero += rand(8..9).to_s + rand(1..9).to_s
      end
  end
  return numero
end

puts "##### CONTACTS"
proponents.each do |p|
  1..2.times do |n|
    Telephone.create(proponent: p, value: (rand(1..2) == 1 ? gContact(:tel) : gContact(:cel)), kind: ((n+1) == 1 ? Contact::OWN[:id] : Contact::OTHER[:id]))
  end
end