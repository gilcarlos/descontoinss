class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.references :city, foreign_key: true
      t.string :public_place
      t.integer :number
      t.string :neighborhood
      t.string :cep

      t.timestamps
    end
  end
end
