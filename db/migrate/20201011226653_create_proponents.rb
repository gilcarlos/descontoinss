class CreateProponents < ActiveRecord::Migration[5.2]
  def change
    create_table :proponents do |t|
      t.references :address, foreign_key: true
      t.string :name
      t.string :cpf
      t.date :date_birth
      t.decimal :salary, precision: 6, scale: 2, null: false
      t.decimal :discount, precision: 5, scale: 2, null: false

      t.timestamps
    end
  end
end
