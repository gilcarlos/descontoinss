class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.references :proponent, foreign_key: true
      t.string :value
      t.string :type
      t.integer :kind

      t.timestamps
    end
  end
end
